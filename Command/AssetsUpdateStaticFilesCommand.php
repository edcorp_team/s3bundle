<?php
/**
 * Created by PhpStorm.
 * User: desarrollador1
 * Date: 16-11-16
 * Time: 11:57 AM
 */

namespace EdcorpTeam\Symfony\Command;

use Aws\S3\S3Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;


class AssetsUpdateStaticFilesCommand extends ContainerAwareCommand
{
    protected $filename;
    protected function configure()
    {
        $this
            ->setname('utils:assets:update-static-files')
            ->setDescription('Update assets static files on S3 storage');
    }
    /**
     * @see Command
     */
    protected function execute(InputInterface $input , OutputInterface $output)
    {
        $output->writeln('<info>Este comando actualizará en S3 todo el contenido de web/bundles</info>');
        $s3client = $this->getContainer()->get('amazon_s3');
        $s3client->registerStreamWrapper();
        $pathFile = $this->getContainer()->get('kernel')->getRootDir() . '/../src/KnowledgeBundle/Resources/public/assets';
        try{
            $s3client->uploadDirectoryToS3($pathFile,"knowledge/assets/");
            $output->writeln('<info>Actualización correctamente</info>');
        }catch(Exception $e){
            $output->writeln('<error>Error</error>');
        }
    }
}
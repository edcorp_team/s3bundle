Require:   "aws/aws-sdk-php-symfony": "~1.0"

Add a composer.json

        "repositories": [
        {
        "type": "vcs",
        "url": "https://bitbucket.org/edcorp_team/s3bundle"
        }
        ],

        "require": {
        ...
        "edcorp_team/s3bundle": "dev-master"
        ...
        }

Composer update

Add a AppKernel

        $bundles = [
        ..
        new EdcorpTeam\Symfony\S3Bundle(),
        ...
        ];
<?php
/**
 * Created by PhpStorm.
 * User: desarrollador1
 * Date: 16-11-16
 * Time: 11:54 AM
 */

namespace EdcorpTeam\Symfony\Entity;

use Aws\S3\S3Client;

class AmazonS3
{
    protected $s3;
    private $bucket;
    private $key;
    public function __construct($key, $secret, $region,$bucket)
    {
        $this->bucket=$bucket;
        $this->key=$key;
        $aws = array(
            'credentials' => array(
                'key'    => $key,
                'secret' => $secret,
            ),
            'region' => $region,
            'version'=> 'latest'
        );
        $this->s3 = new S3Client($aws);
    }
    public function registerStreamWrapper()
    {
        $this->s3->registerStreamWrapper();
    }

    public function getObject($nameObject){
        try {
             $this->s3->getObject(array(
                'Bucket' => $this->bucket,
                'Key' => $nameObject
            ));
            return [1,"Exist"];
        }catch(\Exception $e){
            return get_class($e) == 'Aws\S3\Exception\S3Exception' && $e->getResponse() ?
                [$e->getResponse()->getStatusCode(), $e->getResponse()->getReasonPhrase()] : [0, "Error"];
        }
    }

    public function uploadDirectoryToS3($urlFile){
        $this->s3->uploadDirectory($urlFile,$this->bucket);
    }

    public function uploadDirectoryToS3FromDir($urlFile,$to){
        $this->s3->uploadDirectory($urlFile,$this->bucket,$to);
    }

    public  function uploadFileToS3($path,$urlFile,$nameFile){
        $this->registerStreamWrapper();
        try {
            $result=$this->s3->putObject([
                'Bucket' => $this->bucket,
                'Key'    => $path.$nameFile,
                'Body'   => fopen($urlFile, 'r'),
            ]);
            return true;
        } catch(\Exception $e){
            return false;
        }

    }
}
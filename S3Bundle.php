<?php

namespace EdcorpTeam\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class S3Bundle extends Bundle
{
    const VERSION = '1.0.2';
}
